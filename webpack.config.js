const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const fs = require('fs');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const postcssNormalize = require('postcss-normalize');

const demos = fs.readdirSync(__dirname + '/src/demos')
  .filter((file) => file.includes('.html') || file.includes('.ejs'));

const demosPlugins = demos.map((demo) => {
  return new HtmlWebpackPlugin({
    filename: demo.replace('.ejs', '.html'),
    template: path.resolve(__dirname, 'src', 'demos', demo),
    templateParameters: {
      fs: fs,
    },
  });
});

module.exports = (env, argv) => ({
  entry: path.resolve(__dirname, 'src', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '',
    filename: 'app.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(woff|ttf|eot|woff2|png|jpg|svg)$/,
        loader: "file-loader",

      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          argv.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',

        ],
      },

    ]
  },
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    inline: true,
    contentBase: path.resolve(__dirname, 'src'),
    watchContentBase: true,
    hot: true,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'e-ngineers',
      templateParameters: {
        demos: demos.map((d) => d.replace('.ejs', '.html')),
      },
      template: path.resolve(__dirname, 'src', 'index.ejs')
    }),
    ...demosPlugins,
    new MiniCssExtractPlugin({
      filename: 'app.bundle.css'
    }),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, 'src', 'images'), to: path.resolve(__dirname, 'dist', 'images') }
    ]),
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, 'src', 'js'), to: path.resolve(__dirname, 'dist', 'js') }
    ]),
  ],

});
